var express = require('express'),
	port = 4004,
	http = require('http'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	url = require('url'),
	app = express();

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.listen(port, function(){
	console.info('express server listening on port ' + port);
});


app.get('/search/postalCode', function(req, res){

		var postalCode = url.parse(req.url, true).query.postalCode;
		if(postalCode == '04052030'){
			res.send({
				"postalCode": "04052030",
				"city": "SÃO PAULO",
				"state": "SP",
				"district": "Mirandópolis",
				"address": "RUA ORISSANGA"
			});
		}else if(postalCode == '00000000'){
			res.status(404).send({
				"message": "Endereço não encontrado",
				"errorCode": "2004"
			});
		}else{
			res.status(500).end(postalCode);
		}
});